#include <iostream>
#include <list>
#include <limits.h>
#include <stddef.h>
#include <cstdlib>
#include <sstream>
#include <string.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
#include <ctype.h>
#include <queue>
#include <omp.h>

using namespace std;

double fun (double x) {
   return 1 / (1 + x*x);
   // return x*x;
}

double count (double a, double b) {
    double step = 0.00000001;

    double sum = 0;
    for (double i = a; i < b; i += step) {
        sum += step * fun(i);
    }

    return sum;
}

double count2 (double a, double b) {
    int stepAmount = 100000000;

    double sum = 0;
    double step = (b - a) / stepAmount;

    for (int i = 0; i < stepAmount; i++) {
        sum += step * fun(i*step + a);
    }

    return sum;
}

double parallelCount(double a, double b) {
    double step = 0.00000001;
    double sum = 0;

    int ilosc_rdzeni = 4;
    double result[ilosc_rdzeni];

    #pragma omp parallel
    {
        int aktualny_rdzen = omp_get_thread_num();

        for (double i = a + aktualny_rdzen * step; i < b; i += step * ilosc_rdzeni) {
            result[aktualny_rdzen] += step * fun(i);
        }
    }

    double sum_result = 0;
    for (int i=0; i< ilosc_rdzeni; i++) {
        sum_result += result[i];
    }
    return sum_result;
}

double parallelCount2(double a, double b) {
    double step = 0.00000001;

    int ilosc_rdzeni = 4;
    double result[ilosc_rdzeni];

    #pragma omp parallel
    {
        double sum = 0;
        int aktualny_rdzen = omp_get_thread_num();

        for (double i = a + aktualny_rdzen * step; i < b; i += step * ilosc_rdzeni) {
            sum += step * fun(i);
        }

        result[aktualny_rdzen] = sum;
    }

    double sum_result = 0;
    for (int i=0; i< ilosc_rdzeni; i++) {
        sum_result += result[i];
    }
    return sum_result;
}

double parallelCount3(long a, long b) {
    int stepAmount = 100000000;
    double step = (b - a) / (double)stepAmount;
    cout << "step: " << step << endl;

    double sum = 0;
    int i;
    #pragma omp parallel for reduction (+:sum)
    for (i = 0; i < stepAmount; i++) {
        sum += step * fun(i*step + a);
    }

    return sum;
}

int main()
{
//
//    {
//        cout << omp_get_thread_num() << " " << omp_get_num_threads() << endl;
//    }

    double a=0;
    double b=1;

    long a2=0;
    long b2=1;

    double result = 4 * count(a,b);
    cout << "result: " << result << endl;

    double p_result = 4 * parallelCount(a,b);
    cout << "parallelResult: " << p_result << endl;

    double p_result2 = 4 * parallelCount2(a,b);
    cout << "parallelResult2: " << p_result2 << endl;

    double p_result3 = 4 * parallelCount3(a2,b2);
    cout << "parallelResult3 - with reduction: " << p_result3 << endl;

    return 0;
}
